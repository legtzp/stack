/* 
 * File:   stack.h
 * Author: vagrant
 *
 * Created on February 12, 2015, 1:34 AM
 */

#ifndef STACK_H
#define	STACK_H

typedef struct {
    void * elems;
    int elemSize;
    int length;
    int alloclength;

} Stack;

void StackNew(Stack *s, int elemSize);
void StackPush(Stack *s, void *elemAddr);
void StackPop(Stack *s,void *elemAddr);
void StackDispose(Stack *s);

#endif	/* STACK_H */

/* 
 * File:   stack.c
 * Author: vagrant
 *
 * Created on February 12, 2015, 1:45 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include "stack.h"



void StackNew(Stack *s, int elemSize){
  s->length = 0;
  s->alloclength = 4;
  s->elemSize = elemSize;
  s->elems = malloc(4 * elemSize);
  assert(s->elems != NULL && "Error on StackNew, no more memory");
}

void StackPush(Stack *s, void *elemAddr){
  if (s->length == s->alloclength) {
    (s->alloclength) *= 2;
    s->elems = realloc(s->elems, s->alloclength * s->elemSize);
    assert(s->elems != NULL && "Error on StackPush, no more memory");
  }
  memcpy((char *)s->elems +(s->elemSize * s->length), elemAddr, s->elemSize);
  s->length += 1;
}

void StackPop(Stack *s, void *elemAddr){
  assert(s->length > 0 && "Error No more elements on the Stack");
  s->length -= 1;
  memcpy(elemAddr, (char *) s->elems + (s->elemSize * s->length), s->elemSize);
}

void StackDispose(Stack *s){
  free(s->elems);
}

int main(int argc, char** argv) {
  
  Stack a;
  
  int b = 5;
  int c = 6;
  
  StackNew(&a, 4);
  StackPush(&a, &b);
  StackPush(&a, &c);
  
  int d;
  StackPop(&a, &d);
  
  printf("%d", d);
  
  return (EXIT_SUCCESS);
}


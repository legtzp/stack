/*
 * File:   newcunittest.c
 * Author: vagrant
 *
 * Created on Feb 12, 2015, 1:45:00 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <CUnit/Basic.h>
#include "../stack.h"

int init_suite(void) {
  return 0;
}

int clean_suite(void) {
  return 0;
}

void testStackNew() {
  Stack s;
  StackNew(&s, 5);
  CU_ASSERT(s.length == 0 && 
            s.alloclength == 4 && 
            s.elems != NULL &&
            s.elemSize == 5);
}

void testStackPop() {
  Stack s;
  StackNew(&s, 4);
  
  char* word1 = "Hola";
  char* word2 = "Luis";
  char* word3;
  char* word4;
  
  StackPush(&s, &word1);
  StackPush(&s, &word2);
  
  StackPop(&s, &word3);
  StackPop(&s, &word4);
  
  CU_ASSERT(word3 == word2 && word4 == word1);
}

void testStackPush() {
  Stack s;
  int i;

  StackNew(&s, sizeof(int));
  
  for (i = 0; i < 5; i++)
    StackPush(&s, &i);
  
  CU_ASSERT(s.alloclength == 8 && s.length == 5);
}

int main() {
  CU_pSuite pSuite = NULL;

  /* Initialize the CUnit test registry */
  if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

  /* Add a suite to the registry */
  pSuite = CU_add_suite("newcunittest", init_suite, clean_suite);
  if (NULL == pSuite) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  /* Add the tests to the suite */
  if ((NULL == CU_add_test(pSuite, "testStackNew", testStackNew)) ||
          (NULL == CU_add_test(pSuite, "testStackPop", testStackPop)) ||
          (NULL == CU_add_test(pSuite, "testStackPush", testStackPush))) {
    CU_cleanup_registry();
    return CU_get_error();
  }

  /* Run all tests using the CUnit Basic interface */
  CU_basic_set_mode(CU_BRM_VERBOSE);
  CU_basic_run_tests();
  CU_cleanup_registry();
  return CU_get_error();
}
